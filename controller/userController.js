const {User} =  require("../models")

async function viewAll (req, res) {
    try {
    let data = await User.findAll()
    res.status(200).json({data})
} catch(error) {
    console.log('pencarian semua data tidak ditemukan')
}
}

async function createUser (req, res) {
    try {
    let {username, password} = req.body
    let input = {username, password}
    let data = await User.create(input)
    res.status(201).json(data)
} catch (err) {
    console.log ('tidak dapat membuat user')
}
}

async function updateUser(req,res) {
    try {
    let {id} = req.params;
    let {username, password} = req.body;
    let input = {username, password};
    let data = await User.update(input, {where: {id:id}});
    res.status(200).json(data)
} catch (err) {
    console.log('tidak dapat mengupdate user')
}
}

async function deleteUser(req,res) {
    try {
    let {id} =req.params
    let data = await User.destroy({where: {id:id}})
    res.status(200).json(data)
} catch (err) {
    console.log ('tidak dapat menghapus user')
}
}

async function viewById (req,res) {
    try {
    let {id} = req.params
    let data = await User.findOne ({where: {id: id}})
    res.status(200).json(data)
} catch (err) {
    console.log('tidak dapat mengecek berdasarkan ID')
}
}

module.exports = {viewAll, createUser, updateUser, deleteUser, viewById}