const express = require('express');
const { userController } = require('../controller');
const userRouter = express.Router()

userRouter.get('/', userController.viewAll)
userRouter.post('/', userController.createUser)
userRouter.put('/:id', userController.updateUser)
userRouter.delete('/:id', userController.deleteUser)
userRouter.get('/:id', userController.viewById)

module.exports = userRouter;