const express = require('express');
const userRouter = require('./userRouter');
const router = express.Router()

router.get('/', (req,res) => res.render('home'));
router.use('/users', userRouter)

module.exports = router;
